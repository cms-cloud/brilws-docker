FROM --platform=linux/amd64 python:3.10-bullseye

ARG BRILWS_VERSION=3.9.1

COPY packages.txt packages.txt

RUN apt-get -qq -y update && \
    DEBIAN_FRONTEND=noninteractive apt-get -qq -y install --no-install-recommends $(cat packages.txt) && \
    apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/*


RUN groupadd --gid 1000 bril && \
    adduser --uid 1000 --gid 1000 bril --disabled-password && \
    usermod -aG sudo bril

# Use C.UTF-8 locale to avoid issues with ASCII encoding
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

WORKDIR /home/bril

RUN chown -R bril /home/bril && \
    chmod 755 /home/bril

USER bril
ENV USER bril
ENV HOME /home/bril
ENV PATH="/home/bril/.local/bin:${PATH}"
ENV LD_LIBRARY_PATH="/home/bril/.local/lib/:${LD_LIBRARY_PATH}"

RUN python -m pip install --user --upgrade pip && \
    python -m pip install --user schema prettytable "pandas<2" "numpy<2" "sqlalchemy<2" docopt pyyaml cx_Oracle && \
    python -m pip install --user brilws==${BRILWS_VERSION}

ADD --chown=bril:bril sqlalchemy_frontier/connectors/frontier.py /home/bril/.local/lib/python3.10/site-packages/sqlalchemy/connectors/frontier.py
ADD --chown=bril:bril sqlalchemy_frontier/dialects/oracle/frontier*.py /home/bril/.local/lib/python3.10/site-packages/sqlalchemy/dialects/oracle/

# Need to patch the sqlalchemy dialects to work with frontier
RUN     sed -i '/from . import cx_oracle  # noqa/a from . import frontier' /home/bril/.local/lib/python3.10/site-packages/sqlalchemy/dialects/oracle/__init__.py

# Install frontier client
RUN wget -q http://frontier.cern.ch/dist/frontier_client__2.10.2__src.tar.gz && \
    tar xzf frontier_client__2.10.2__src.tar.gz && \
    cd frontier_client__2.10.2__src && \
    wget -q https://raw.githubusercontent.com/manugarg/pacparser/52e88ea956a91f927ab1491012482b9250c72494/src/pacparser.h && \
    sed -i 's/^\(COPT.*\)/\1 -fcommon/' Makefile && \
    make && \
    make install && \
    cp -r dist/* $HOME/.local/ && \
    cd .. && \
    rm -rf frontier_client__2.10.2__src.tar.gz frontier_client__2.10.2__src

# Install Oracle instant client
RUN wget -q https://download.oracle.com/otn_software/linux/instantclient/217000/instantclient-basiclite-linux.x64-21.7.0.0.0dbru.zip && \
    unzip instantclient-basiclite-linux.x64-21.7.0.0.0dbru.zip && \
    cp instantclient_21_7/*.so* $HOME/.local/lib && \
    rm -r instantclient_21_7

CMD ["/bin/bash"]